PREFIX = "https://jptechnical.freshdesk.com/support/solutions/articles/"

URLS = [
  "36000014953-how-do-i-take-a-screenshot-",
  "36000014959-how-do-i-do-a-speed-test-",
  "36000020846-do-i-really-have-a-virus-how-to-spot-a-fake-",
  "36000028337-setting-up-an-openvpn-connection-file-for-tunnelblick",
  "36000030567-how-do-i-find-my-computer-name-",
  "36000031184-how-do-i-use-an-openvpn-config-file-with-tunnelblick-on-my-mac-",
  "36000034975-where-do-i-find-my-quickbooks-license-and-registration-information-",
  "36000035496-what-if-windows-blocks-the-downloaded-app-",
  "36000035921-how-do-i-open-a-file-with-a-different-app-",
  "36000038249-how-to-download-and-install-watchdog-by-jp-technical"
]

WGET = "wget -m --html-extension "
SAVEDIR = "-P /Volumes/hdd/Users/jp/git/HowTos/RubyScrape/ "

URLS.each do |url|
  system WGET + SAVEDIR + PREFIX + url
end
